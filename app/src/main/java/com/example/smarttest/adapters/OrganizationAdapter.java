package com.example.smarttest.adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.smarttest.R;
import com.example.smarttest.activities.DetailOrganizationActivity;
import com.example.smarttest.models.Organization;

import java.util.ArrayList;

public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.ViewHolder> {
    Activity activity;
    ArrayList<Organization> organizations;

    public OrganizationAdapter(Activity activity, ArrayList<Organization> organizations) {
        this.activity = activity;
        this.organizations = organizations;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_organization, viewGroup, false);
        return new OrganizationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Organization organization = organizations.get(i);
        holder.lblOrganiztionName.setText(organization.getName());
        holder.encriptionKey.setText(activity.getText(R.string.encription_key) + " " + organization.getEncriptionCode());
        OnClick click = new OnClick();
        click.setRawInfo(organization.getRawDetail());
        holder.llMainInfo.setOnClickListener(click);
    }

    @Override
    public int getItemCount() {
        return organizations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView lblOrganiztionName, encriptionKey;
        LinearLayout llMainInfo;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            lblOrganiztionName  = itemView.findViewById(R.id.lbl_organization_name);
            encriptionKey       = itemView.findViewById(R.id.encription_key);
            llMainInfo          = itemView.findViewById(R.id.ll_main_info);
        }
    }

    class OnClick implements View.OnClickListener {
        String rawInfo;

        public String getRawInfo() {
            return rawInfo;
        }

        public void setRawInfo(String rawInfo) {
            this.rawInfo = rawInfo;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ll_main_info:
                    Intent intent = new Intent(activity, DetailOrganizationActivity.class);
                    intent.putExtra("json", getRawInfo());
                    activity.startActivity(intent);
                    break;
            }
        }
    }
}
