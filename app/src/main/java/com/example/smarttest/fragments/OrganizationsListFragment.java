package com.example.smarttest.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smarttest.R;
import com.example.smarttest.adapters.OrganizationAdapter;
import com.example.smarttest.models.Organization;
import com.example.smarttest.webservices.OrganizationWebservice;

import java.util.ArrayList;

public class OrganizationsListFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    ProgressBar loader;
    LinearLayout llList;
    OrganizationWebservice organizationWebservice;
    ArrayList<Organization> organizations;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_organizations_list, container, false);
        init();
        return view;
    }

    private void init(){
        recyclerView    = view.findViewById(R.id.recycler);
        loader          = view.findViewById(R.id.loader);
        llList          = view.findViewById(R.id.ll_list);
        organizationWebservice  = new OrganizationWebservice(getActivity());
        new OnLoad().execute();

    }

    private class OnLoad extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);
            llList.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.GONE);
            llList.setVisibility(View.VISIBLE);
            if (!organizationWebservice.getErrorMessage().equals("")){
                Toast.makeText(getActivity(), organizationWebservice.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
            if (organizations.size() == 0){
                Toast.makeText(getActivity(), R.string.there_are_no_organizations, Toast.LENGTH_LONG).show();
            } else {
                OrganizationAdapter adapter = new OrganizationAdapter(getActivity(), organizations);
                recyclerView.setAdapter(adapter);
                GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
                recyclerView.setLayoutManager(layoutManager);
                adapter.notifyDataSetChanged();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            organizations = organizationWebservice.getOrganizations();
            return null;
        }
    }
}
