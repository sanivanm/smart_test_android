package com.example.smarttest.constants;

public class Apiconstants {
    public static final String URL = "https://smart.ivanmv.com/";
    public static final String ANDROID_PLATFORM = "1";
    public static final String LOGIN = Apiconstants.URL + "api/auth/login";
    public static final String SUCCESS_MESSAGE = "success";
    public static final String GET_ORGANIZATIONS = Apiconstants.URL + "api/auth/get_organizations";
    public static final String SAVE_ORGANIZATION = Apiconstants.URL + "api/auth/save_organization";
    public static final String SAVE_USER = Apiconstants.URL + "api/auth/signup";
}
