package com.example.smarttest;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smarttest.activities.MainMenuActivity;
import com.example.smarttest.activities.RegisterActivity;
import com.example.smarttest.webservices.UserWebservice;

public class MainActivity extends InitClass implements View.OnClickListener {

    EditText txtEmail, txtPassword;
    Button btnEntrar;
    ProgressBar loader;
    LinearLayout llView;
    UserWebservice userWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    protected void init(){
        txtEmail    = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        btnEntrar   = findViewById(R.id.btn_entrar);
        loader      = findViewById(R.id.loader);
        llView      = findViewById(R.id.ll_view);
        userInformation.saveToken("");
        userInformation.saveUserInformation("");
        userWebservice  = new UserWebservice(getActivity());
        btnEntrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_entrar:
                new OnEnter().execute();
                break;
            case R.id.register_new_user:
                Intent intent = new Intent(getActivity(), RegisterActivity.class);
                startActivity(intent);
                break;
        }
    }

    private class OnEnter extends AsyncTask<Void, Void, Void>{

        boolean logged = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);
            llView.setVisibility(View.GONE);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.GONE);
            llView.setVisibility(View.VISIBLE);
            if (logged){
                startActivity(new Intent(getActivity(), MainMenuActivity.class));
            }else{
                Toast.makeText(getActivity(), userWebservice.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            logged = userWebservice.login(extractText(txtEmail), extractText(txtPassword));
            return null;
        }
    }

}
