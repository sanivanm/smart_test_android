package com.example.smarttest.shared;

import android.app.Activity;
import android.content.SharedPreferences;

import com.example.smarttest.models.User;

import org.json.JSONException;
import org.json.JSONObject;

public class UserInformation extends InitShared{

    private static final String USER_INFO = "userInfo";
    private static final String TOKEN = "token";

    public UserInformation(Activity activity) {
        super(activity, "UserInformation");
    }

    public void saveUserInformation(String user) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(USER_INFO, user);
        editor.commit();
    }

    public void saveToken(String access_token) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(TOKEN, "Bearer " + access_token);
        editor.commit();
    }

    public String getToken(){
        return getSharedPreferences().getString(TOKEN, "");
    }

    public User getUser(){
        User user = new User();
        try {
            JSONObject json = new JSONObject(getSharedPreferences().getString(USER_INFO,""));
            user.setId(json.getInt("id"));
            user.setName(json.getString("name"));
            user.setEmail(json.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }
}
