package com.example.smarttest.models;

import org.json.JSONObject;

public class Organization {
    private int id;
    private int userId;
    private String name;
    private String address;
    private String phone;
    private String external_id;
    private String encriptionCode;
    private String rawDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getEncriptionCode() {
        return encriptionCode;
    }

    public void setEncriptionCode(String encriptionCode) {
        this.encriptionCode = encriptionCode;
    }

    public String getRawDetail() {
        return rawDetail;
    }

    public void setRawDetail(String rawDetail) {
        this.rawDetail = rawDetail;
    }
}
