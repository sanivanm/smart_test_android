package com.example.smarttest.webservices;

import android.app.Activity;
import android.util.Log;

import com.example.smarttest.constants.Apiconstants;
import com.example.smarttest.models.User;
import com.example.smarttest.shared.UserInformation;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UserWebservice extends InitWebservice {
    UserInformation userInformation;

    public UserWebservice(Activity activity) {
        super(activity);
        userInformation = new UserInformation(activity);
        TAG = "UserWebservice_test";
    }

    public boolean login(String email, String password){
        boolean logged = false;
        RequestBody body = new FormBody.Builder()
                .add( "email", email)
                .add("password", password)
                .add("plataforma", Apiconstants.ANDROID_PLATFORM)
                .build();


        Request newReq = new Request.Builder()
                .url(Apiconstants.LOGIN)
                .post(body)
                .build();
        try {
            Response response = client.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(Apiconstants.SUCCESS_MESSAGE)) {
                    userInformation.saveToken(json.getString("access_token"));
                    userInformation.saveUserInformation(json.getString("user"));
                    logged = true;
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return logged;
    }

    public boolean register(User user) {
        boolean saved = false;
        RequestBody body = new FormBody.Builder()
                .add( "name", user.getName())
                .add("email", user.getEmail())
                .add("password", user.getPassword())
                .add("password_confirmation", user.getConfirm())
                .add("plataforma", Apiconstants.ANDROID_PLATFORM)
                .build();
        Request newReq = new Request.Builder()
                .url(Apiconstants.SAVE_USER)
                .post(body)
                .build();
        try {
            Response response = client.newCall(newReq).execute();
            String jsonReponse = response.body().string();
            Log.i(TAG, "json_response:" + jsonReponse);
            JSONObject json = new JSONObject(jsonReponse);
            this.setErrorMessage("");
            if(json.has("status")) {
                String serverStatus = json.getString("status");
                if (serverStatus.equals(Apiconstants.SUCCESS_MESSAGE)) {
                    login(user.getEmail(), user.getPassword());
                    saved = true;
                } else {
                    setErrorMessage(json.getString("message"));
                }
            } else {
                setErrorMessage(json.getString("message"));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return saved;
    }
}
