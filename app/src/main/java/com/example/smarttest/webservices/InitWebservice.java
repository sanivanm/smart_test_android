package com.example.smarttest.webservices;

import android.app.Activity;

import com.example.smarttest.shared.UserInformation;

import okhttp3.OkHttpClient;


public class InitWebservice {
    protected Activity activity;
    protected String errorMessage = "";
    protected String TAG = "initWebservice";
    protected OkHttpClient client = new OkHttpClient();
    protected UserInformation userInformation;

    public InitWebservice(Activity activity) {
        this.activity = activity;
        userInformation = new UserInformation(activity);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getTAG() {
        return TAG;
    }
    public void setTAG(String TAG){
        this.TAG = TAG;
    }
}
