package com.example.smarttest.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smarttest.InitClass;
import com.example.smarttest.R;
import com.example.smarttest.models.Organization;
import com.example.smarttest.webservices.OrganizationWebservice;

public class RegisterOrganizationActivity extends InitClass {

    EditText txtName, txtAddress, txtPhone, txtEncriptionCode;
    Button btnSave;
    ProgressBar loader;
    LinearLayout llForm;
    OrganizationWebservice organizationWebservice;
    Organization organization = new Organization();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void init() {
        TAG = "RegisterOrganization_test";
        setContentView(R.layout.activity_register_organization);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loader              = findViewById(R.id.loader);
        txtName             = findViewById(R.id.txt_name);
        txtAddress          = findViewById(R.id.txt_address);
        txtPhone            = findViewById(R.id.txt_phone);
        txtEncriptionCode   = findViewById(R.id.txt_encription_code);
        btnSave             = findViewById(R.id.btn_save);
        llForm              = findViewById(R.id.ll_form);
        organizationWebservice  = new OrganizationWebservice(getActivity());
        btnSave.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save:
                new OnSave().execute();
                break;
        }
    }

    class OnSave extends AsyncTask<Void, Void, Void> {
        boolean saved = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loader.setVisibility(View.VISIBLE);
            llForm.setVisibility(View.GONE);

            organization.setName(extractText(txtName));
            organization.setAddress(extractText(txtAddress));
            organization.setEncriptionCode(extractText(txtEncriptionCode));
            organization.setPhone(extractText(txtPhone));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            loader.setVisibility(View.GONE);
            llForm.setVisibility(View.VISIBLE);
            if (saved){
                Toast.makeText(getActivity(), R.string.saved, Toast.LENGTH_LONG).show();
                getActivity().onBackPressed();
            }else{
                Toast.makeText(getActivity(), R.string.an_error_ocurred, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            saved = organizationWebservice.saveOrganization(organization);
            return null;
        }
    }
}
