package com.example.smarttest.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.smarttest.InitClass;
import com.example.smarttest.MainActivity;
import com.example.smarttest.R;
import com.example.smarttest.models.User;
import com.example.smarttest.webservices.UserWebservice;

public class RegisterActivity extends InitClass implements View.OnClickListener {
    EditText txtName, txtEmail, txtPassword, txtConfirm;
    ProgressBar loader;
    LinearLayout llForm;
    UserWebservice userWebservice;
    User user = new User();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void init() {
        setContentView(R.layout.activity_register);
        txtName     = findViewById(R.id.txt_name);
        txtEmail    = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        txtConfirm  = findViewById(R.id.txt_confirm);
        loader      = findViewById(R.id.loader);
        llForm      = findViewById(R.id.ll_form);
        userWebservice  = new UserWebservice(getActivity());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save:
                new OnRegsiter().execute();
                break;
        }
    }

    class OnRegsiter extends AsyncTask<Void, Void, Void>{

        boolean saved = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            llForm.setVisibility(View.GONE);
            loader.setVisibility(View.VISIBLE);
            user.setName(extractText(txtName));
            user.setEmail(extractText(txtEmail));
            user.setPassword(extractText(txtPassword));
            user.setConfirm(extractText(txtConfirm));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            llForm.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
            if (saved){
                Intent intent = new Intent(getActivity(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), userWebservice.getErrorMessage(), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            saved = userWebservice.register(user);
            return null;
        }
    }
}
