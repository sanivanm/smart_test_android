package com.example.smarttest.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.smarttest.InitClass;
import com.example.smarttest.R;
import com.example.smarttest.models.Organization;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailOrganizationActivity extends InitClass implements View.OnClickListener {

    EditText txtCode;
    Button btnAcept;
    Organization organization = new Organization();
    LinearLayout llCode, llDetail;
    TextView lblName, lblAddress, lblPhone, lblExternalId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void init() {
        TAG = "DetailOrganization_test";
        setContentView(R.layout.activity_detail_organization);
        Log.i(TAG, getIntent().getStringExtra("json"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnAcept    = findViewById(R.id.btn_accept);
        txtCode     = findViewById(R.id.txt_code);
        llCode      = findViewById(R.id.ll_code);
        llDetail    = findViewById(R.id.ll_detail);
        lblName     = findViewById(R.id.lbl_name);
        lblAddress  = findViewById(R.id.lbl_address);
        lblExternalId   = findViewById(R.id.lbl_external_id);
        lblPhone    = findViewById(R.id.lbl_phone);

        btnAcept.setOnClickListener(this);
        try {
            JSONObject j = new JSONObject(getIntent().getStringExtra("json"));

            organization.setId(j.getInt("id"));
            organization.setUserId(j.getInt("userId"));
            organization.setName(j.getString("name"));
            organization.setAddress(j.getString("address"));
            organization.setPhone(j.getString("phone"));
            organization.setExternal_id(j.getString("external_id"));
            organization.setEncriptionCode(j.getString("encriptionCode"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_accept:
                if (organization.getEncriptionCode().equals(extractText(txtCode))){
                    llCode.setVisibility(View.GONE);
                    llDetail.setVisibility(View.VISIBLE);
                    lblName.setText(organization.getName());
                    lblPhone.setText(organization.getPhone());
                    lblAddress.setText(organization.getAddress());
                    lblExternalId.setText(organization.getExternal_id());
                } else {
                    txtCode.setError(getString(R.string.there_is_an_error_with_the_code));
                }
                break;
        }
    }
}
