package com.example.smarttest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.smarttest.InitClass;
import com.example.smarttest.MainActivity;
import com.example.smarttest.R;
import com.example.smarttest.fragments.OrganizationsListFragment;
import com.example.smarttest.shared.UserInformation;

import org.w3c.dom.Text;

public class MainMenuActivity extends InitClass
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager fragmentManager, externFragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void init() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);

        TextView txtEmail = headerView.findViewById(R.id.lbl_email_menu);
        TextView txtUserName = headerView.findViewById(R.id.lbl_user_name);

        txtEmail.setText(userInformation.getUser().getEmail());
        txtUserName.setText(userInformation.getUser().getName());

        navigationView.setNavigationItemSelectedListener(this);

        setFirstSegment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_organization_list) {
            setFirstSegment();
        } else if (id == R.id.nav_organization_signup) {
            Intent intent = new Intent(getActivity(), RegisterOrganizationActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_session_close) {
            userInformation.saveToken("");
            userInformation.saveUserInformation("");
            Intent intent = new Intent(getActivity(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setFirstSegment(){
        fragmentManager = getSupportFragmentManager();
        externFragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        OrganizationsListFragment missionsList = new OrganizationsListFragment();
        fragmentTransaction.replace(R.id.main_content, missionsList);
        fragmentTransaction.commit();
        getSupportFragmentManager().getPrimaryNavigationFragment();
    }

    @Override
    public void onClick(View v) { }

    @Override
    protected void onRestart() {
        super.onRestart();
        getActivity().recreate();
    }
}
